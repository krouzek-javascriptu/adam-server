import http from 'node:http'

const server = http.createServer((request, res) => {
    console.log(request)

    if (request.url == '/json') {
        res.writeHead(200, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify({
            data: 'Hello World!'
        }));
    } else if (request.url == '/txt') {
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Hi from txt')
    } else {
        res.writeHead(404)
        res.end()
    }
});


server.listen(80); 